#!/usr/bin/python3

from gi.repository import Gio, GLib

NAME = 'org.gnome.SettingsDaemon.Smartcard'
IFACE = NAME
MANAGER_PATH = '/org/gnome/SettingsDaemon/Smartcard/Manager'

connection = Gio.bus_get_sync(Gio.BusType.SESSION, None)

def get_obect_props(object_path, iface):
    props_proxy = Gio.DBusProxy.new_sync(connection, Gio.DBusProxyFlags.NONE,
        None, NAME, object_path, 'org.freedesktop.DBus.Properties', None)

    return props_proxy.GetAll('(s)', iface)

manager = Gio.DBusProxy.new_sync(connection, Gio.DBusProxyFlags.NONE, None,
    NAME, MANAGER_PATH, IFACE + '.Manager', None)

def print_token_info(token):
    indent_step = 2
    print('{}{}'.format(' ' * indent_step, token))

    def print_object_props(path, iface, indent):
        for name, value in get_obect_props(path, iface).items():
            print('{}{}: {}'.format(' ' * indent, name, value))
            if name == 'Driver' and value and value != '/':
                print_object_props(value, IFACE + '.Driver', indent + indent_step)

    print_object_props(token, IFACE + '.Token', indent_step * 2)
    print()

print('Inserted tokens:')
[print_token_info(token) for token in manager.GetInsertedTokens()]

print('Login token:')
try:
    print_token_info(manager.GetLoginToken())
except GLib.Error as e:
    if 'org.gnome.SettingsDaemon.Smartcard.Manager.Error.FindingSmartcard' in str(e):
        print('  No login token set via $PKCS11_LOGIN_TOKEN_NAME')
    else:
        raise(e)
